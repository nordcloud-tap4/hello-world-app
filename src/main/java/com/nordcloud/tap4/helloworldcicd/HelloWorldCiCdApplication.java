package com.nordcloud.tap4.helloworldcicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloWorldCiCdApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloWorldCiCdApplication.class, args);
	}

}
