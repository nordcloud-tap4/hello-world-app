package com.nordcloud.tap4.helloworldcicd.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Muneer Syed
 */
@Controller
public class WelcomeController {

	@GetMapping("/")
	public String index() {
		return "redirect:/welcome";
	}

	@GetMapping("/welcome")
	public String welcome(@RequestParam(name = "name", required = false, defaultValue = "") String name, Model model) {
		model.addAttribute("visitor", name);

		return "welcome";
	}

}
